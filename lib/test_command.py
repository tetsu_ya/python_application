# -*- coding: utf-8 -*-

import sys
import argparse
import inspect

from load_config import load_config

DEFAULT_CONFIG = {
    "meta_data": {
        "program_name": "test_command",
        "author": "your name",
        "last_release_date": "2017.12.01",
        "version": "0.0.1",
    }
}


def _parse_sub_command1(sub_parsers):
    parser = sub_parsers.add_parser('sub_command1')
    parser.add_argument('-a', '--all')
    parser.set_defaults(func=_exe_sub_command1)
    return sub_parsers


def _exe_sub_command1(args, confs):
    import sub_command1
    sub_command1.sub_command1(args.all)


def _parse_sub_command2(sub_parsers):
    parser = sub_parsers.add_parser('sub_command2')
    parser.add_argument('-b', '--bill')
    parser.set_defaults(func=_exe_sub_command2)
    return sub_parsers


def _exe_sub_command2(args, confs):
    import sub_command2
    sub_command2.sub_command2(args.bill)


def main():
    parser = argparse.ArgumentParser()
    sub_parsers = parser.add_subparsers()

    # execute _parse_*** function and set sub-command parser
    parse_function_prefix = "_parse_"
    for function_name, function_object in inspect.getmembers(sys.modules["__main__"], inspect.isfunction):
        if function_name[:len(parse_function_prefix)] == parse_function_prefix:
            sub_parsers = function_object(sub_parsers)

    # parse arguments
    args = parser.parse_args()

    # load config
    confs = load_config(DEFAULT_CONFIG)

    # execute sub-command function
    if hasattr(args, 'func'):
        args.func(args, confs)
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
