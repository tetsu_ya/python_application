# -*- coding: utf-8 -*-

import sys
import os
import copy
import collections
import yaml


def merge_dict(dct, merge_dct):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, merge_dict recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    referenced by [https://gist.github.com/angstwad/bf22d1822c38a92ec0a9]
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.items():
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], collections.Mapping)):
            merge_dict(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]


def _load_and_add_config(conf_obj, conf_path):
    if os.path.isfile(conf_path):
        with open(conf_path, "r") as fin:
            conf_obj1 = yaml.safe_load(fin)
        merge_dict(conf_obj, conf_obj1)
        return True
    else:
        return False


def load_config(default_conf, conf_path="", env_name=None):
    config_obj = copy.deepcopy(default_conf)

    program_title = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    config_filename = program_title + ".yml"

    # プログラムディレクトリから
    config_path1 = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), config_filename)
    if not _load_and_add_config(config_obj, config_path1):
        # プログラムディレクトリに設定ファイルが存在しない場合は、デフォルト設定で保存する
        yaml.safe_dump(default_conf, open(config_path1, "w"), default_flow_style=False)

    # ホームディレクトリから
    config_path2 = os.path.join(os.path.expanduser("~"), config_filename)
    _load_and_add_config(config_obj, config_path2)

    # 環境変数から
    if env_name:
        try:
            config_path3 = os.environ[env_name]
            _load_and_add_config(config_obj, config_path3)
        except KeyError:
            print("ENVIRON is not found")

    # パラメータから
    config_path4 = conf_path
    _load_and_add_config(config_obj, config_path4)

    return config_obj


def main():
    print(load_config({"program": "test", "meta": {"author": "your", "version": "0.0.1"}}))


if __name__ == "__main__":
    main()
