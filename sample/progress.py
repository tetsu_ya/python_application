# -*- coding: utf-8 -*-

import sys
import time


def main():
    print("program start")
    for i in range(500):
        try:
            if i % 10 == 0:
                sys.stdout.write("\r%d" % i)
                sys.stdout.flush()
            if i == 200:  # error print test
                raise Exception("200")
        except Exception as e:
            sys.stdout.write("\r")
            print("error: %s" % e)
        time.sleep(0.01)
    sys.stdout.write("\r")
    print("program end")


if __name__ == "__main__":
    main()
