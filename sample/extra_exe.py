# -*- coding: utf-8 -*-

"""対話型コマンドを外部実行するプログラムサンプル
- 特殊な対話型コマンドを外部実行するサンプル。
- 例えば、パスワードの入力を必要とするコマンドを外部実行し、自動的にパスワードを入力させる。
"""

import sys
import argparse
import getpass

import pexpect
from pexpect.popen_spawn import PopenSpawn

PASSWORD = "1234"


# テスト用の対話型コマンドプログラム
def exec_input():
    # パスワード入力を求める
    print("test input command")
    input_password = input("input password:")
    # input_password = getpass.getpass("input password:")  # うまくいかない
    if input_password == PASSWORD:  # パスワードが一致する場合
        print("success")
    else:  # パスワードが一致しない場合
        print("error")


def main():
    # 引数解析
    # [input]サブコマンドを指定すると、テスト用の対話型コマンドプログラムとして実行される
    # サブコマンドが指定されないと、対話型コマンドプログラムを外部実行するサンプルプログラムとして実行される
    arg_parser = argparse.ArgumentParser()
    sub_parsers = arg_parser.add_subparsers()
    input_parser = sub_parsers.add_parser("input")
    input_parser.set_defaults(func=exec_input)
    args = arg_parser.parse_args()

    # サブコマンドが指定されている場合の処理([input]サブコマンドしか実装されていない)
    if hasattr(args, 'func'):
        args.func()  # 実質exec_input関数が実行される
    # サブコマンドが指定されない場合のメイン処理
    else:
        # このプログラム自身を[input]サブコマンドを指定しての外部実行
        p = PopenSpawn("python extra_exe.py input", encoding="utf-8")
        p.logfile = sys.stdout  # open("test.txt", "w")  # 外部実行の出力をそのまま標準出力に表示
        p.expect("input password:")  # "input password:"という表示がされたら待機
        p.sendline("1234")  # 入力待機へ"1234"文字列を入力
        p.expect(pexpect.EOF)  # 外部実行を最後まで読み込む


if __name__ == "__main__":
    main()
