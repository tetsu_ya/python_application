# -*- coding: utf-8 -*-

import logging


def main():
    logger_a = logging.getLogger("log_a")
    logger_a_handler = logging.StreamHandler()
    logger_a_handler.setFormatter(logging.Formatter("[%(asctime)s] <%(name)s> (%(levelname)s) \"%(message)s\""))
    logger_a.addHandler(logger_a_handler)
    logger_a.setLevel(logging.INFO)
    logger_a.propagate = False

    logger_b = logging.getLogger("log_a.test")
    logger_b_handler = logging.StreamHandler()
    logger_b_handler.setFormatter(logging.Formatter("[%(asctime)s] <%(name)s> (%(levelname)s) \"%(message)s\""))
    logger_b.addHandler(logger_b_handler)
    logger_b.propagate = False

    logger_a.error("a")
    logger_b.error("b")

if __name__ == "__main__":
    main()
